# Sample - React & Redux

> Sample web-app client built with React and Redux

### About

This project is built on top of a ReduxJS stack, using ReactJS as UI, {customized} Twitter Bootstrap v4 theme as CSS framework

> https://reactjs.org/

> https://redux.js.org/

> https://getbootstrap.com/

More information on predictability, unidirectional data-flow, engine state, stores, actions and reducers (core concepts of Reducts) can be found online.

It is a good start to read if you are not familiar with such concepts :

> https://redux.js.org/docs/introduction/

### Requirements

#### Web Server

This project is a web-application, and therefore require a web server, capable of serving static files (actually just an `index.html` file)

Feel free to use the one you prefer : Nginx, Apache, a NodeJS web-server such as Express etc.

If you plan to host this web-application somewhere, it might be interesting to be able to execute NodeJS commands on the server, although this is not required.

PS : in local environment, you can use `webpack-dev-server`. (see HMR section)

#### Dependencies

This project uses Yarn as package manager, but NPM can be used instead (just replace `yarn` by `npm` in the next commands).

> https://yarnpkg.com/en/

This project also uses Webpack as assets bundler. It will be automatically installed as a dependency, you have nothing specific to install on your system.

> https://webpack.js.org/

### Installation

Before starting, create and edit the local environment config file :

```
# config file
cp config/prod.env.js config/dev.env.js
```

``` js
# It should contains something like
module.exports = {
    NODE_ENV: '"development"',
    API_BASE_URL: '"http://localhost:9000/api/v1/"'
}
```

Then install dependencies with either yarn (or NPM)

``` bash
# install dependencies (--> will build the project as well)
yarn install
```

### HMR

React

``` bash
# Start Hot Module Replacement
yarn run watch
```

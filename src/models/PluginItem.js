import { Record } from 'immutable'

export default Record(
    {
        id: null,
        created_at: null,
        name: null,
        is_active: false,
        description: null,
        category: null,
        tags: [],
        requests: [],
        answers: []
    },
    'PluginItem'
)

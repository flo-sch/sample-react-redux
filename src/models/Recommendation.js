import { Record } from 'immutable'

export default Record(
    {
        id: null,
        created_at: null,
        intent: null,
        plugin: null,
        traitment: 0
    },
    'Recommendation'
)

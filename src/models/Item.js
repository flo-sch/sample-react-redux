import { Record } from 'immutable'

export default Record(
    {
        id: null,
        created_at: null,
        name: null,
        token: null,
        users: 0,
        description: null,
        quote: null
    },
    'Item'
)

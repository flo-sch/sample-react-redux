import { Record } from 'immutable'

export default Record(
    {
        id: null,
        created_at: null,
        username: null,
        firstname: null,
        lastname: null,
        email: null,
        phone_number: null,
        picture: null,
        address: null,
        subscription: null
    },
    'User'
)

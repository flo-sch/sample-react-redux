import * as Types from '@/actions/Application'

/**
 * ATTENTION
 *
 * The "state" object below MUST BE IMMUTABLE
 *
 * It MUST NOT BE directly updated.
 * Instead, replace it with an updated copy of itself, which is the purpose of the Object Spread Operator.
 *
 * @see https://redux.js.org/docs/faq/ImmutableData.html#why-is-immutability-required
 */
const initialState = {
    isSidebarOpen: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case Types.DISPLAY_SIDEBAR:
            return {
                ...state,
                isSidebarOpen: true
            }
        case Types.HIDE_SIDEBAR:
            return {
                ...state,
                isSidebarOpen: false
            }
        default:
            return state
    }
}

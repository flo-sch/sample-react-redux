import * as Types from '@/actions/Plugins'

/**
 * ATTENTION
 *
 * The "state" object below MUST BE IMMUTABLE
 *
 * It MUST NOT BE directly updated.
 * Instead, replace it with an updated copy of itself, which is the purpose of the Object Spread Operator.
 *
 * @see https://redux.js.org/docs/faq/ImmutableData.html#why-is-immutability-required
 */
const initialState = {
    items: [],
    error: ''
}

export default (state = initialState, action) => {
    switch (action.type) {
        case Types.ITEMS_FETCH_SUCCESS:
            return {
                ...state,
                items: action.items
            }
        case Types.ITEMS_FETCH_FAILURE:
            return {
                ...state,
                error: action.error
            }
        default:
            return state
    }
}

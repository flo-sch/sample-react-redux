import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { i18nReducer } from 'react-redux-i18n'
import { reducer as formReducer } from 'redux-form'

import ApplicationReducer from '@/reducers/ApplicationReducer'
import PluginsReducer from '@/reducers/PluginsReducer'
import UserReducer from '@/reducers/UserReducer'
import XmlHttpRequestLoaderReducer from '@/reducers/XmlHttpRequestLoaderReducer'

export default combineReducers({
    app: ApplicationReducer,
    user: UserReducer,
    plugins: PluginsReducer,
    xhr: XmlHttpRequestLoaderReducer,
    routing: routerReducer,
    i18n: i18nReducer,
    form: formReducer
})

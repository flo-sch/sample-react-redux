import * as Types from '@/actions/XmlHttpRequest'

/**
 * ATTENTION
 *
 * The "state" object below MUST BE IMMUTABLE
 *
 * It MUST NOT BE directly updated.
 * Instead, replace it with an updated copy of itself, which is the purpose of the Object Spread Operator.
 *
 * @see https://redux.js.org/docs/faq/ImmutableData.html#why-is-immutability-required
 */
const initialState = {
    isLoading: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case Types.XHR_REQUEST_START:
            return {
                ...state,
                isLoading: true
            }
        case Types.XHR_REQUEST_END:
            return {
                ...state,
                isLoading: false
            }
        default:
            return state
    }
}

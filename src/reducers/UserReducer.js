import { isFuture } from 'date-fns'

import * as Types from '@/actions/User'
import Storage from '@/middlewares/PersistentStorage'
import User from '@/models/User'

const getStoredToken = () => Storage.get('token')

const getStoredUser = () => {
    let user = null

    const storedUser = Storage.get('user')

    if (storedUser) {
        user = new User(JSON.parse(storedUser))
    }

    return user
}

/**
 * Always false, Unless we have a way to check token validity.
 *
 * For instance, if the expiration date is given when we get the token,
 * We could compare it here.
 */
const isTokenValid = () => {
    let isTokenValid = false
    let expirationDate = Storage.get('token_expiration_date')

    if (expirationDate) {
        isTokenValid = isFuture(expirationDate)
    }

    return isTokenValid
}

/**
 * ATTENTION
 *
 * The "state" object below MUST BE IMMUTABLE
 *
 * It MUST NOT BE directly updated.
 * Instead, replace it with an updated copy of itself, which is the purpose of the Object Spread Operator.
 *
 * @see https://redux.js.org/docs/faq/ImmutableData.html#why-is-immutability-required
 */
const initialState = {
    displayLoginForm: false,
    isAuthenticated: isTokenValid(),
    token: getStoredToken(),
    user: getStoredUser(),
    error: ''
}

export default (state = initialState, action) => {
    switch (action.type) {
        case Types.DISPLAY_LOGIN_FORM:
            return {
                ...state,
                displayLoginForm: true
            }
        case Types.HIDE_LOGIN_FORM:
            return {
                ...state,
                displayLoginForm: false
            }
        case Types.AUTHENTICATION_SUCCESS:
            return {
                ...state,
                isAuthenticated: true,
                user: action.user,
                token: action.token,
                error: ''
            }
        case Types.AUTHENTICATION_ERROR:
            return {
                ...state,
                isAuthenticated: false,
                user: null,
                token: null,
                error: action.error
            }
        case Types.DISCONNECTION:
            return {
                ...state,
                isAuthenticated: false,
                user: null,
                token: null,
                error: null
            }
        case Types.EDIT_INFORMATIONS_SUCCESS:
            return {
                ...state,
                user: action.user,
                error: null
            }
        case Types.EDIT_INFORMATIONS_FAILURE:
            return {
                ...state,
                error: action.error
            }
        case Types.EDIT_SUBSCRIPTION_SUCCESS:
            return {
                ...state,
                user: action.user,
                error: null
            }
        case Types.EDIT_SUBSCRIPTION_FAILURE:
            return {
                ...state,
                error: action.error
            }
        default:
            return state
    }
}

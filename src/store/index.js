import { routerMiddleware } from 'react-router-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import { createLogger } from 'redux-logger'
/**
 * Short story : thunks are one simple way of dealing with asynchronous functions with reducers.
 *
 * @see https://stackoverflow.com/questions/35411423/how-to-dispatch-a-redux-action-with-a-timeout/35415559#35415559
 */
import thunkMiddleware from 'redux-thunk'

import HistoryMiddleware from '@/middlewares/History'
import reducers from '@/reducers'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export default createStore(
    reducers,
    composeEnhancers(
        applyMiddleware(thunkMiddleware, createLogger(), routerMiddleware(HistoryMiddleware))
    )
)

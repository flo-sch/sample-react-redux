export const ROUTE_REGEXP = '/:locale(en|fr)/:path*'
export const LOCALES = ['en', 'fr']

export const generateUrl = (locale, location) => {
    let { pathname } = location
    pathname = pathname.startsWith('/') ? pathname.substring(1) : pathname
    const [routeLocale, ...segments] = pathname.split('/')

    return `/${locale}/${
        LOCALES.includes(routeLocale) ? segments.join('/') : location.pathname
    }`.replace(/\/+$/, '')
}

export const extractCurrentLocale = location => {
    let { pathname } = location
    pathname = pathname.startsWith('/') ? pathname.substring(1) : pathname
    const [locale] = pathname.split('/')

    return LOCALES.includes(locale) ? locale : null
}

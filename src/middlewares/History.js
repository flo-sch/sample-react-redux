/**
 * Browser history uses native browsers HTML5 history API
 * @see https://developer.mozilla.org/en-US/docs/Web/API/History_API
 *
 * However, it does not work if the app is served with the file protocol :
 * file:///path-to-your-app/index.html
 * In such case, you would need to use Hash History instead.
 * Which would prepend URLs with a # such as index.html/#/ for the homepage.
 */
import * as History from 'history'
import { setLocale } from 'react-redux-i18n'

import Store from '@/store'
import { extractCurrentLocale } from '@/utils/UrlGenerator'
import { hideSidebar } from '@/actions/Application'

// const history =  History.createHashHistory()
const history = History.createBrowserHistory()

// Automatically dispatch hideSidebar action
history.listen(location => {
    Store.dispatch(setLocale(extractCurrentLocale(location)))
    return Store.dispatch(hideSidebar)
})

export default history

import axios from 'axios'

import Store from '@/store'
import { startRequest, endRequest } from '@/actions/XmlHttpRequest'

/**
 * ApiClient
 *
 * This class is an HTTP client bootstrap for any external WebService or API
 * It needs to implement the different calls required by the application, always returning Promise objects.
 * Internally, it uses Axios as a base HTTP client.
 *
 * @see https://www.npmjs.com/package/axios
 */
class ApiClient {
    constructor(baseUrl) {
        this.instance = axios.create({
            baseURL: baseUrl
            // timeout: 30000,
            // headers: {}
        })

        this.instance.interceptors.request.use(
            () => {
                Store.dispatch(startRequest())
            },
            () => {
                Store.dispatch(endRequest())
            }
        )

        this.instance.interceptors.response.use(
            () => {
                Store.dispatch(endRequest())
            },
            () => {
                Store.dispatch(endRequest())
            }
        )
    }

    authenticate = (email, password) =>
        this.instance.post('/authenticate', {
            email,
            password
        })

    editProfile = (firstName, lastName, email, phoneNumber, address) =>
        this.instance.put('/profile/update', {
            first_name: firstName,
            last_name: lastName,
            email,
            phone_number: phoneNumber,
            address
        })

    editSubscription = paymentToken =>
        this.instance.put('/subscription/update', {
            payment_token: paymentToken
        })

    addPlugin = id =>
        this.instance.post('/profile/plugins/add', {
            id
        })

    fetchPlugins = (tags, query) =>
        this.instance.get('/profile/plugins', {
            tags,
            query
        })
}

// baseURL is a parameter, injected at compilation by webpack.
// @see /config/prod.env.js
export default new ApiClient(process.env.API_BASE_URL)

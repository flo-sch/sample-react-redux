import fr from '@/translations/fr'
import en from '@/translations/en'

export const translations = {
    en,
    fr
}

export default translations

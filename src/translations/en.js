export default {
    xhr: {
        loader: {
            text: '.. Loading ..'
        }
    },
    sidebar: {
        menu: {
            links: {
                home: 'Home page',
                documentation: 'Documentation',
                dashboard: 'Dashboard',
                tools: 'Tools',
                plugins: 'Plugins',
                account: 'My Account'
            }
        }
    },
    home: {
        baseline: 'An amazing services platform<br>ready to use for whatever',
        login_form: {
            placeholders: {
                email: 'email',
                password: '**********'
            },
            errors: {
                email: 'This field is mandatory',
                password: 'This field is mandatory',
                authentication: {
                    error: 'An error has occured. Authentication failed.',
                    failure: 'Invalid credentials.'
                }
            },
            actions: {
                submit: 'Login',
                close: 'Close'
            }
        },
        actions: {
            login: 'Login',
            sign_up: 'Sign Up'
        }
    },
    dashboard: {
        title: 'Dashboard',
        analytics: {
            title: 'Analytics',
            caption: 'Lorem graph lorem ipsum'
        },
        statistics: {
            use: {
                title: 'Some statistics'
            },
            services: {
                title: 'Other statistics'
            }
        }
    },
    tools: {
        title: 'Stress Test',
        stress_test: {
            gauge: {
                caption: 'Success rate'
            },
            actions: {
                submit: 'Stress Test'
            }
        },
        recommendations: {
            title: 'Recommendation',
            table_head: {
                intent: 'Intent',
                plugin: 'Plugin',
                traitment: '% traitment'
            },
            actions: {
                plugin: 'Plugin'
            }
        }
    },
    plugins: {
        title: 'Plugins',
        item: {
            created_at: 'Created on',
            actions: {
                on: 'ON',
                off: 'OFF'
            }
        }
    },
    account: {
        title: 'My account',
        informations: {
            header: 'Your informations'
        },
        subscription: {
            header: 'Your subscription'
        },
        chatbots: {
            title: 'My items'
        }
    }
}

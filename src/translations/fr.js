export default {
    xhr: {
        loader: {
            text: '.. Chargement ..'
        }
    },
    sidebar: {
        menu: {
            links: {
                home: 'Accueil',
                documentation: 'Documentation',
                dashboard: 'Dashboard',
                tools: 'Outils',
                plugins: 'Plugins',
                account: 'Mon Compte'
            }
        }
    },
    home: {
        baseline: "Une magnifique platforme de services<br>prêts à l'usage pour ce que vous voulez",
        login_form: {
            placeholders: {
                email: 'e-mail',
                password: '**********'
            },
            errors: {
                email: 'Ce champ est obligatoire',
                password: 'Ce champ est obligatoire',
                authentication: {
                    error: 'Une erreur est survenue. Identification impossible.',
                    failure: 'Identifiants invalides.'
                }
            },
            actions: {
                submit: 'Connexion',
                close: 'Fermer'
            }
        },
        actions: {
            login: 'Connexion',
            sign_up: 'Inscription'
        }
    },
    dashboard: {
        title: 'Dashboard',
        analytics: {
            title: 'Analyse',
            caption: 'Lorem graph lorem ipsum'
        },
        statistics: {
            use: {
                title: 'Des statistiques'
            },
            services: {
                title: "D'autres statistiques"
            }
        }
    },
    tools: {
        title: 'Stress Test',
        stress_test: {
            gauge: {
                caption: 'Taux de succès'
            },
            actions: {
                submit: 'Stress Test'
            }
        },
        recommendations: {
            title: 'Recommandation',
            table_head: {
                intent: 'Intent',
                plugin: 'Plugin',
                traitment: '% traités'
            },
            actions: {
                plugin: 'Plugin'
            }
        }
    },
    plugins: {
        title: 'Plugins',
        item: {
            created_at: 'Créé le',
            actions: {
                on: 'ON',
                off: 'OFF'
            }
        }
    },
    account: {
        title: 'Mon compte',
        informations: {
            header: 'Vos informations'
        },
        subscription: {
            header: 'Votre abonnement'
        },
        chatbots: {
            title: 'Mes items'
        }
    }
}

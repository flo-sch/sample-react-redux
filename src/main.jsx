/**
 * Application style
 */
import '@/assets/sass/app.scss'

import React from 'react'
import { render } from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import { syncTranslationWithStore, loadTranslations, setLocale } from 'react-redux-i18n'
import History from '@/middlewares/History'

import { extractCurrentLocale } from '@/utils/UrlGenerator'
import { translations } from '@/translations'
import Store from '@/store'
import Root from '@/containers/Root'

syncTranslationWithStore(Store)
Store.dispatch(loadTranslations(translations))
Store.dispatch(setLocale(extractCurrentLocale(History.location)))

const mountPoint = document.getElementById('app-container')

const renderApp = Component =>
    render(
        <AppContainer>
            <Component store={Store} history={History} />
        </AppContainer>,
        mountPoint
    )

renderApp(Root)

// Webpack Hot Module Replacement API
if (module.hot) {
    module.hot.accept('@/containers/Root', () => {
        renderApp(Root)
    })
}

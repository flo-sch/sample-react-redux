import PropTypes from 'prop-types'
import * as React from 'react'
import { Provider } from 'react-redux'
import { Redirect, Route, Switch } from 'react-router-dom'
import { ConnectedRouter } from 'react-router-redux'

import App from '@/containers/App'

class Root extends React.Component {
    render() {
        const { history, store } = this.props

        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Route path="/:locale(en|fr)" component={App} />
                        <Redirect to="/en" />
                    </Switch>
                </ConnectedRouter>
            </Provider>
        )
    }
}

Root.propTypes = {
    history: PropTypes.object.isRequired,
    store: PropTypes.object.isRequired
}

export default Root

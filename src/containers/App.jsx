import classNames from 'classnames'
import PropTypes from 'prop-types'
import * as React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import Router from '@/containers/Router'
import Sidebar from '@/components/Sidebar'
import XHRLoader from '@/components/Loader/XHRLoader'

class App extends React.Component {
    render() {
        const { isSidebarOpen, match } = this.props

        return (
            <div
                id="app"
                className={classNames({
                    'container-fluid': true,
                    'sidebar-open': isSidebarOpen,
                    full: match.isExact
                })}
            >
                <div className="row">
                    <div className="sidebar-col col-sm-4 col-md-3">
                        <Sidebar />
                    </div>
                    <div className="col">
                        <Router />
                    </div>
                </div>
                <XHRLoader />
            </div>
        )
    }
}

App.propTypes = {
    isSidebarOpen: PropTypes.bool.isRequired,
    match: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    isSidebarOpen: state.app.isSidebarOpen
})

export default withRouter(connect(mapStateToProps, null)(App))

import PropTypes from 'prop-types'
import * as React from 'react'
import { Redirect, Route, Switch, withRouter } from 'react-router-dom'

import Home from '@/components/Home'
import Dashboard from '@/components/Dashboard'
import Tools from '@/components/Tools'
import Plugins from '@/components/Plugins'
import Account from '@/components/Account'

class Router extends React.Component {
    render() {
        const { match } = this.props

        return (
            <Switch>
                <Route exact path={`${match.url}/`} component={Home} />
                <Route path={`${match.url}/dashboard`} component={Dashboard} />
                <Route path={`${match.url}/tools`} component={Tools} />
                <Route path={`${match.url}/plugins`} component={Plugins} />
                <Route path={`${match.url}/account`} component={Account} />
                <Redirect to={`${match.url}`} />
            </Switch>
        )
    }
}

Router.propTypes = {
    match: PropTypes.object.isRequired
}

export default withRouter(Router)

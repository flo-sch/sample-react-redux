/**
 * Action types
 */
export const XHR_REQUEST_START = '@APP/XHR_REQUEST_START'
export const XHR_REQUEST_END = '@APP/XHR_REQUEST_END'

/**
 * Actions
 */
const onRequestStart = () => ({
    type: XHR_REQUEST_START
})

const onRequestEnd = () => ({
    type: XHR_REQUEST_END
})

/**
 * Action creators
 */
export const startRequest = () => dispatch => dispatch(onRequestStart())

export const endRequest = () => dispatch => dispatch(onRequestEnd())

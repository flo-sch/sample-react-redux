/**
 * Action types
 */
export const DISPLAY_SIDEBAR = '@APP/DISPLAY_SIDEBAR'
export const HIDE_SIDEBAR = '@APP/HIDE_SIDEBAR'

/**
 * Actions creators
 */
export const displaySidebar = {
    type: DISPLAY_SIDEBAR
}

export const hideSidebar = {
    type: HIDE_SIDEBAR
}

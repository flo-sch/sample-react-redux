import { I18n } from 'react-redux-i18n'

import ApiClient from '@/middlewares/ApiClient'

/**
 * Action types
 */
export const ITEMS_FETCH_SUCCESS = '@PLUGINS/ITEMS_FETCHED'
export const ITEMS_FETCH_FAILURE = '@PLUGINS/ITEMS_FETCH_FAILURE'
export const ADD_ITEM_SUCCESS = '@PLUGINS/ADD_ITEM_SUCCESS'
export const ADD_ITEM_FAILURE = '@PLUGINS/ADD_ITEM_FAILURE'

/**
 * Actions
 */
const onFetchSuccess = items => ({
    type: ITEMS_FETCH_SUCCESS,
    items
})

const onFetchFailure = error => ({
    type: ITEMS_FETCH_FAILURE,
    error
})

const onAddItemSuccess = item => ({
    type: ADD_ITEM_SUCCESS,
    item
})

const onAddItemFailure = error => ({
    type: ADD_ITEM_FAILURE,
    error
})

/**
 * Actions creators
 */
export const search = ({ tag, query }, dispatch) =>
    ApiClient.fetchPlugins(tag, query)
        .then(result => {
            let items = []

            if (result) {
                items = result.items
            }

            return dispatch(onFetchSuccess(items))
        })
        .catch(error => {
            return dispatch(onFetchFailure(error))
        })

export const addItem = ({ id }, dispatch) =>
    ApiClient.addPlugin(id)
        .then(result => {
            if (result) {
                return dispatch(onAddItemSuccess(result.item))
            } else {
                return dispatch(onAddItemFailure(I18n.t('plugins.add_item.failure')))
            }
        })
        .catch(error => {
            return dispatch(onAddItemFailure(error))
        })

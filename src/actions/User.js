import { I18n } from 'react-redux-i18n'
import { push } from 'react-router-redux'

import ApiClient from '@/middlewares/ApiClient'
import Storage from '@/middlewares/PersistentStorage'

/**
 * Timeout before resetting authentication error
 */
let AUTHENTICATION_ERROR_TIMEOUT = null
export const AUTHENTICATION_ERROR_DURATION = 5000

/**
 * Action types
 */
export const DISPLAY_LOGIN_FORM = '@USER/DISPLAY_LOGIN_FORM'
export const HIDE_LOGIN_FORM = '@USER/HIDE_LOGIN_FORM'
export const AUTHENTICATION_SUCCESS = '@USER/AUTHENTICATION_SUCCESS'
export const AUTHENTICATION_ERROR = '@USER/AUTHENTICATION_ERROR'
export const DISCONNECTION = '@USER/DISCONNECTION'
export const EDIT_INFORMATIONS_SUCCESS = '@USER/EDIT_INFORMATIONS_SUCCESS'
export const EDIT_INFORMATIONS_FAILURE = '@USER/EDIT_INFORMATIONS_FAILURE'
export const EDIT_SUBSCRIPTION_SUCCESS = '@USER/EDIT_SUBSCRIPTION_SUCCESS'
export const EDIT_SUBSCRIPTION_FAILURE = '@USER/EDIT_SUBSCRIPTION_FAILURE'

/**
 * Actions
 */
export const displayLoginForm = {
    type: DISPLAY_LOGIN_FORM
}

export const hideLoginForm = {
    type: HIDE_LOGIN_FORM
}

const onAuthenticationSuccess = (token, user) => ({
    type: AUTHENTICATION_SUCCESS,
    token,
    user
})

const onAuthenticationError = error => ({
    type: AUTHENTICATION_ERROR,
    error
})

const dispatchAuthenticationError = (error, dispatch) => {
    if (AUTHENTICATION_ERROR_TIMEOUT) {
        clearTimeout(AUTHENTICATION_ERROR_TIMEOUT)
        AUTHENTICATION_ERROR_TIMEOUT = null
    }

    AUTHENTICATION_ERROR_TIMEOUT = setTimeout(() => {
        dispatch(onAuthenticationError(null))
    }, AUTHENTICATION_ERROR_DURATION)

    return dispatch(onAuthenticationError(error))
}

const onEditInformationsSuccess = user => ({
    type: EDIT_INFORMATIONS_SUCCESS,
    user
})

const onEditInformationsFailure = error => ({
    type: EDIT_INFORMATIONS_FAILURE,
    error
})

const onEditSubscriptionSuccess = user => ({
    type: EDIT_SUBSCRIPTION_SUCCESS,
    user
})

const onEditSubscriptionFailure = error => ({
    type: EDIT_SUBSCRIPTION_FAILURE,
    error
})

/**
 * Action creators
 */
export const authenticate = ({ email, password }, dispatch) =>
    ApiClient.authenticate(email, password)
        .then(result => {
            if (result) {
                const { token, user } = result

                if (token && user) {
                    Storage.set('token', token)
                    Storage.set('user', JSON.stringify(user))

                    dispatch(onAuthenticationSuccess(token, user))
                    return dispatch(push('/'))
                } else {
                    return dispatchAuthenticationError(
                        I18n.t('home.login_form.errors.authentication.failure'),
                        dispatch
                    )
                }
            } else {
                return dispatchAuthenticationError(
                    I18n.t('home.login_form.errors.authentication.error'),
                    dispatch
                )
            }
        })
        .catch(error => {
            return dispatchAuthenticationError(error, dispatch)
        })

export const editInformations = ({ firstName, lastName, email, phoneNumber, address }, dispatch) =>
    ApiClient.editProfile(firstName, lastName, email, phoneNumber, address)
        .then(result => {
            if (result) {
                const { user } = result

                if (user) {
                    Storage.set('user', JSON.stringify(user))

                    return dispatch(onEditInformationsSuccess(user))
                } else {
                    return dispatch(
                        onEditInformationsFailure(I18n.t('account.edit_informations.failure'))
                    )
                }
            } else {
                return dispatch(
                    onEditInformationsFailure(I18n.t('account.edit_informations.failure'))
                )
            }
        })
        .catch(error => {
            return dispatch(onEditInformationsFailure(error))
        })

export const editSubscription = ({ paymentToken }, dispatch) =>
    ApiClient.editSubscription(paymentToken)
        .then(result => {
            if (result) {
                const { user } = result

                if (user) {
                    Storage.set('user', JSON.stringify(user))

                    return dispatch(onEditSubscriptionSuccess(user))
                } else {
                    return dispatch(
                        onEditSubscriptionFailure(I18n.t('account.edit_subscription.failure'))
                    )
                }
            } else {
                return dispatch(
                    onEditSubscriptionFailure(I18n.t('account.edit_subscription.failure'))
                )
            }
        })
        .catch(error => {
            return dispatch(onEditSubscriptionFailure(error))
        })

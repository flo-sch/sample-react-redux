import PropTypes from 'prop-types'
import * as React from 'react'
import { Button, Card, Col, Row, Table } from 'reactstrap'
import { connect } from 'react-redux'
import { Translate } from 'react-redux-i18n'
import { Link } from 'react-router-dom'

import GaugeGraph from '@/components/Graph/GaugeGraph'
import Recommendation from '@/models/Recommendation'

class Tools extends React.Component {
    render() {
        const { locale, successRate, recommendations } = this.props

        return (
            <section id="tools" className="section">
                <Translate tag="h2" value="tools.title" className="section-title" />
                <Row>
                    <Col xs="12" lg="8">
                        <GaugeGraph value={successRate} />
                    </Col>
                </Row>
                <Translate
                    tag="h3"
                    value="tools.recommendations.title"
                    className="section-subtitle"
                />
                <Card outline color="secondary" className="card-table">
                    <Table>
                        <thead>
                            <tr>
                                <Translate
                                    tag="th"
                                    value="tools.recommendations.table_head.intent"
                                />
                                <Translate
                                    tag="th"
                                    value="tools.recommendations.table_head.plugin"
                                />
                                <Translate
                                    tag="th"
                                    value="tools.recommendations.table_head.traitment"
                                />
                            </tr>
                        </thead>
                        <tbody>
                            {recommendations.map((recommendation, index) => (
                                <tr key={index}>
                                    <td>{recommendation.intent}</td>
                                    <td>{recommendation.plugin}</td>
                                    <td>
                                        <span className="table-figure">
                                            {recommendation.traitment}
                                            <sup>%</sup>
                                        </span>
                                        <Button
                                            tag={Link}
                                            to={`/${locale}/plugins`}
                                            color="primary"
                                            size="sm"
                                            className="btn-table-action"
                                        >
                                            <Translate value="tools.recommendations.actions.plugin" />
                                        </Button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Card>
            </section>
        )
    }
}

Tools.propTypes = {
    locale: PropTypes.string.isRequired,
    successRate: PropTypes.number.isRequired,
    recommendations: PropTypes.arrayOf(PropTypes.instanceOf(Recommendation)).isRequired
}

const mapStateToProps = state => ({
    locale: state.i18n.locale,
    successRate: Math.round(Math.random() * 100),
    recommendations: [
        new Recommendation({
            id: 1,
            intent: 'Franchise garage',
            plugin: 'Item 1',
            traitment: 79
        }),
        new Recommendation({
            id: 2,
            intent: 'Crypto-tank Traps',
            plugin: 'Item 2',
            traitment: 85
        }),
        new Recommendation({
            id: 3,
            intent: 'Order-Flow Refrigerators',
            plugin: 'Item 3',
            traitment: 64
        }),
        new Recommendation({
            id: 3,
            intent: 'Semiotics Hotdogs',
            plugin: 'Item 4',
            traitment: 98
        })
    ]
})

export default connect(mapStateToProps, null)(Tools)

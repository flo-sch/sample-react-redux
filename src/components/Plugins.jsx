import PropTypes from 'prop-types'
import * as React from 'react'
import { Col, Row } from 'reactstrap'
import { connect } from 'react-redux'
import { Translate } from 'react-redux-i18n'

import { search } from '@/actions/Plugins'
import PluginsFiltersForm from '@/components/Form/PluginsFiltersForm'
import PluginCard from '@/components/Plugins/PluginCard'
import PluginItem from '@/models/PluginItem'

class Plugins extends React.Component {
    render() {
        const { items, search } = this.props

        return (
            <section id="plugins" className="section">
                <Translate tag="h2" value="plugins.title" className="section-title" />
                <PluginsFiltersForm onSubmit={search} />
                <hr />
                <Row>
                    {items.map((item, index) => (
                        <Col xs="12" md="6" key={index}>
                            <PluginCard item={item} />
                        </Col>
                    ))}
                </Row>
            </section>
        )
    }
}

Plugins.propTypes = {
    error: PropTypes.string,
    search: PropTypes.func.isRequired,
    items: PropTypes.array.isRequired
}

const mapStateToProps = state => ({
    error: state.plugins.error,
    // items: state.plugins.items,
    items: [
        new PluginItem({
            id: 1,
            name: 'Item 1',
            created_at: '2018-01-01T00:00:00+01:00',
            is_active: true,
            description:
                'Post-rifle narrative chrome order-flow saturation point modem numinous faded bicycle weathered geodesic man fluidity katana refrigerator. Neural j-pop silent youtube office alcohol DIY. Geodesic sign bridge kanji stimulate silent carbon city katana media network table shrine cyber-crypto.',
            category: 'favorites',
            tags: ['abc', 'def', 'ghi', 'jkl', 'mno'],
            requests: [],
            answers: []
        }),
        new PluginItem({
            id: 2,
            name: 'Item 2',
            created_at: '2018-01-01T00:00:00+01:00',
            is_active: false,
            description:
                'Fetishism boy saturation point numinous katana gang Shibuya savant tanto decay. Soul-delay cartel render-farm rebar knife skyscraper tiger-team nano-systemic. Otaku jeans warehouse silent camera cyber-crypto-katana assault bicycle narrative sign concrete render-farm post-neural.',
            category: 'helpers',
            tags: ['abc', 'jkl', 'mno'],
            requests: [],
            answers: []
        }),
        new PluginItem({
            id: 3,
            name: 'Item 3',
            created_at: '2018-01-01T00:00:00+01:00',
            is_active: false,
            description:
                'Nodal point wonton soup drone hacker sub-orbital augmented reality drugs DIY-ware weathered rain vinyl free-market chrome market futurity. Silent gang nodal point artisanal towards range-rover Shibuya shoes. Tanto dissident hacker knife drone shoes nodality wristwatch rebar city Legba.',
            category: 'favorites',
            tags: ['def', 'mno'],
            requests: [],
            answers: []
        }),
        new PluginItem({
            id: 4,
            name: 'Item 4',
            created_at: '2018-01-01T00:00:00+01:00',
            is_active: true,
            description:
                'Youtube artisanal denim narrative rebar 3D-printed pre-geodesic nodality beef noodles film towards stimulate human.-space sub-orbital pistol construct silent city futurity. Man human decay woman car systema computer lights boat table pistol corrupted carbon sign concrete dolphin assault.',
            category: 'other',
            tags: ['abc', 'jkl', 'mno'],
            requests: [],
            answers: []
        })
    ]
})

const mapDispatchToProps = dispatch => ({
    search: ({ tag, query }) => dispatch(search(tag, query))
})

export default connect(mapStateToProps, mapDispatchToProps)(Plugins)

import classNames from 'classnames'
import PropTypes from 'prop-types'
import * as React from 'react'
import Loader from 'react-loaders'
import { connect } from 'react-redux'
import { Translate } from 'react-redux-i18n'

class XHRLoader extends React.Component {
    render() {
        const { isAnyRequestLoading } = this.props

        return (
            <aside
                id="xhr-loader"
                className={classNames({
                    'd-flex justify-content-center align-items-center': true,
                    'is-displayed': isAnyRequestLoading
                })}
            >
                <div className="text-center">
                    <h3>
                        <Translate value="xhr.loader.text" />
                    </h3>
                    <br className="w-100" />
                    <div className="d-inline-block">
                        <Loader type="ball-clip-rotate-multiple" />
                    </div>
                </div>
            </aside>
        )
    }
}

XHRLoader.propTypes = {
    isAnyRequestLoading: PropTypes.bool.isRequired
}

const mapStateToProps = state => ({
    isAnyRequestLoading: state.xhr.isLoading
})

export default connect(mapStateToProps, null)(XHRLoader)

import PropTypes from 'prop-types'
import * as React from 'react'
import { connect } from 'react-redux'
import { Button } from 'reactstrap'
import { Translate } from 'react-redux-i18n'

import { displayLoginForm, authenticate } from '@/actions/User'
import LoginForm from '@/components/Form/LoginForm'

class Home extends React.Component {
    render() {
        const { displayLoginForm } = this.props

        return (
            <section id="home" className="section d-flex justify-content-center align-items-center">
                <div>
                    <LoginForm onSubmit={authenticate} />
                    <Button
                        color="link"
                        size="lg"
                        id="login-button"
                        onClick={displayLoginForm}
                        className="text-secondary"
                    >
                        <Translate value="home.actions.login" className="d-inline-block" />
                    </Button>
                    <div className="clearfix">
                        <Translate
                            tag="h1"
                            value="home.baseline"
                            className="app-baseline"
                            dangerousHTML
                        />
                        <Button
                            color="secondary"
                            size="lg"
                            className="mt-2"
                            onClick={displayLoginForm}
                        >
                            <Translate
                                value="home.actions.sign_up"
                                className="d-inline-block pt-4 px-3 pb-2"
                            />
                        </Button>
                    </div>
                </div>
            </section>
        )
    }
}

Home.propTypes = {
    displayLoginForm: PropTypes.func.isRequired,
    authenticate: PropTypes.func.isRequired
}

const mapDispatchToProps = dispatch => ({
    displayLoginForm: () => dispatch(displayLoginForm),
    authenticate: ({ email, password }) => dispatch(authenticate(email, password))
})

export default connect(null, mapDispatchToProps)(Home)

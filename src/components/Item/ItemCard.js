import PropTypes from 'prop-types'
import * as React from 'react'
import { Button, Card, CardBody, CardFooter, CardText, CardTitle } from 'reactstrap'

import Item from '@/models/Item'

class ItemCard extends React.Component {
    render() {
        const { item } = this.props

        return (
            <Card outline color="primary" className="subscription h-100 mb-4">
                <CardBody>
                    <div className="d-flex justify-content-between align-items-center">
                        <CardTitle>{item.name}</CardTitle>
                        <div>
                            <small className="prepend-line">Token: {item.token}</small>
                            <small className="ml-2 prepend-line">Users : {item.users}</small>
                        </div>
                    </div>
                    <CardText className="mt-2 mb-0">{item.description}</CardText>
                </CardBody>
                <CardFooter className="card-footer-quote">
                    <blockquote className="card-quote">{item.quote}</blockquote>
                </CardFooter>
                <div className="card-bottom-actions">
                    <Button color="primary">
                        <span className="fa fa-pencil" />
                    </Button>
                    <Button color="primary">
                        <span className="fa fa-remove" />
                    </Button>
                </div>
            </Card>
        )
    }
}

ItemCard.propTypes = {
    item: PropTypes.instanceOf(Item).isRequired
}

export default ItemCard

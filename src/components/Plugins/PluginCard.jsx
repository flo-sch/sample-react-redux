import PropTypes from 'prop-types'
import * as React from 'react'
import { Badge, Button, Card, CardText, CardTitle } from 'reactstrap'
import { Translate, Localize } from 'react-redux-i18n'

import PluginItem from '@/models/PluginItem'

class PluginCard extends React.Component {
    get icon() {
        const { item } = this.props

        switch (item.category) {
            case 'helpers':
                return <span className="fa fa-thumbs-o-up" />
            case 'favorites':
                return <span className="fa fa-star-o" />
        }

        return <span className="fa fa-commenting-o" />
    }

    render() {
        const { item } = this.props

        return (
            <Card
                body
                outline
                color={item.is_active ? 'primary' : 'dark'}
                className="card-strict mb-4"
            >
                <figure className="card-category-icon">{this.icon}</figure>
                <CardTitle>{item.name}</CardTitle>
                <small className="prepend-line">
                    <Translate value="plugins.item.created_at" />
                    &nbsp;
                    <Localize value={item.created_at} dateFormat="L" />
                </small>
                <CardText className="mt-2">{item.description}</CardText>
                <div className="card-tags">
                    {item.tags.map((tag, index) => (
                        <Badge color="light" key={index}>
                            {tag}
                        </Badge>
                    ))}
                </div>
                <div className="card-bottom-actions">
                    <Button color="primary">
                        <Translate
                            value={
                                item.is_active
                                    ? 'plugins.item.actions.on'
                                    : 'plugins.item.actions.off'
                            }
                        />
                    </Button>
                </div>
            </Card>
        )
    }
}

PluginCard.propTypes = {
    item: PropTypes.instanceOf(PluginItem).isRequired
}

export default PluginCard

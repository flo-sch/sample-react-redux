import PropTypes from 'prop-types'
import * as React from 'react'
import { Button, Card, CardText, CardTitle, Col, Row } from 'reactstrap'
import { connect } from 'react-redux'
import { Translate, Localize } from 'react-redux-i18n'

import ItemCard from '@/components/Item/ItemCard'
import Item from '@/models/Item'

class Account extends React.Component {
    render() {
        const { items } = this.props

        return (
            <section id="account" className="section">
                <Translate tag="h2" value="account.title" className="section-title" />
                <Row className="mb-5 mb-md-3">
                    <Col xs="12" md="6">
                        <Card body outline color="dark" className="informations h-100">
                            <Translate
                                tag="small"
                                value="account.informations.header"
                                className="card-introduction"
                            />
                            <CardTitle>John Doe</CardTitle>
                            <CardText>
                                <a
                                    href="mailto:john.doe@example.com"
                                    className="text-primarary text-underline"
                                >
                                    john.doe@example.com
                                </a>
                                <br />
                                06 00 00 00 00
                                <br />
                                SOME COMPANY
                                <br />
                                Paris, 75000
                            </CardText>
                            <div className="card-bottom-actions">
                                <Button color="primary">
                                    <Translate value="account.informations.actions.edit" />
                                </Button>
                            </div>
                        </Card>
                    </Col>
                    <Col xs="12" md="6">
                        <Card body outline color="dark" className="subscription h-100 mt-4 mt-md-0">
                            <Translate
                                tag="small"
                                value="account.subscription.header"
                                className="card-introduction"
                            />
                            <CardTitle>Subscription</CardTitle>
                            <CardText>
                                Renewal date :{' '}
                                <Localize tag="strong" value="2020-12-31" dateFormat="L" />
                            </CardText>
                            <div className="card-bottom-actions">
                                <Button color="primary">
                                    <Translate value="account.subscription.actions.edit" />
                                </Button>
                            </div>
                        </Card>
                    </Col>
                </Row>
                <h3 className="d-flex align-items-center section-subtitle">
                    <Translate value="account.chatbots.title" />
                    <Button color="secondary" className="ml-2 rounded-circle">
                        <span className="fa fa-plus" />
                    </Button>
                </h3>
                {items.map((item, index) => <ItemCard key={index} item={item} />)}
            </section>
        )
    }
}

Account.propTypes = {
    items: PropTypes.arrayOf(PropTypes.instanceOf(Item)).isRequired
}

const mapStateToProps = state => ({
    items: [
        new Item({
            name: 'WALL-E',
            token: '8260 LA20W50',
            users: '10 - 50',
            description:
                'Warehouse man concrete chrome systemic table corporation fluidity marketing. BASE jump engine garage drone crypto-face forwards 3D-printed tube.',
            quote:
                'Weathered post-nodality cyber-convenience store wristwatch vinyl narrative advert media meta-girl long-chain hydrocarbons. Otaku alcohol boat construct digital rifle dolphin skyscraper. Construct convenience store drone dissident meta-courier realism lights. '
        }),
        new Item({
            name: 'LOREM IPSUM',
            token: '9504 XL98GW12',
            users: '20 - 30',
            description:
                'Footage beef noodles long-chain hydrocarbons woman hotdog warehouse meta-market soul-delay papier-mache garage.',
            quote:
                'Urban city systemic drugs A.I. cartel weathered girl math-numinous tube dome wonton soup render-farm boat. A.I. San Francisco sprawl sentient shoes knife beef noodles papier-mache crypto-motion singularity numinous range-rover.'
        })
    ]
})

export default connect(mapStateToProps, null)(Account)

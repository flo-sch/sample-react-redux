import PropTypes from 'prop-types'
import * as React from 'react'
import { connect } from 'react-redux'
import { Translate } from 'react-redux-i18n'
import { NavLink, withRouter } from 'react-router-dom'
import { Button, ListGroup } from 'reactstrap'

import Logo from '@/assets/images/logo-white.svg'
import LocaleSelector from '@/components/Locale/LocaleSelector'
import { displaySidebar, hideSidebar } from '@/actions/Application'

class Sidebar extends React.Component {
    get header() {
        const { displaySidebar, hideSidebar, isSidebarOpen, user } = this.props
        let userContent = null

        if (user) {
            userContent = (
                <div className="d-flex justify-content-between align-items-end">
                    <div>
                        <h4 id="user-firstname">{user.firstname}</h4>
                        <p id="user-lastname">{user.lastname}</p>
                    </div>
                </div>
            )
        }

        return (
            <header id="sidebar-header" className="d-flex justify-content-md-center">
                <Button
                    id="sidebar-toggle"
                    color="primary"
                    onClick={isSidebarOpen ? hideSidebar : displaySidebar}
                >
                    <span className="sidebar-menu-bar" />
                    <span className="sidebar-menu-bar" />
                    <span className="sidebar-menu-bar" />
                </Button>
                <figure
                    id="sidebar-logo"
                    className="d-flex justify-content-center align-items-center"
                >
                    <img src={Logo} width={169} height={44} />
                </figure>
                {userContent}
            </header>
        )
    }

    render() {
        const { match } = this.props

        return (
            <aside id="sidebar">
                {this.header}
                <div className="mt-md-4">
                    <ListGroup id="sidebar-menu" className="border-0">
                        <NavLink
                            exact
                            to={`${match.url}`}
                            className="list-group-item bg-transparent border-0"
                            activeClassName="active"
                        >
                            <Translate value="sidebar.menu.links.home" />
                        </NavLink>
                        <NavLink
                            to="#"
                            className="list-group-item bg-transparent border-0"
                            activeClassName="active"
                        >
                            <Translate value="sidebar.menu.links.documentation" />
                        </NavLink>
                        <NavLink
                            to={`${match.url}/dashboard`}
                            className="list-group-item bg-transparent border-0"
                            activeClassName="active"
                        >
                            <Translate value="sidebar.menu.links.dashboard" />
                        </NavLink>
                        <NavLink
                            to={`${match.url}/tools`}
                            className="list-group-item bg-transparent border-0"
                            activeClassName="active"
                        >
                            <Translate value="sidebar.menu.links.tools" />
                        </NavLink>
                        <NavLink
                            to={`${match.url}/plugins`}
                            className="list-group-item bg-transparent border-0"
                            activeClassName="active"
                        >
                            <Translate value="sidebar.menu.links.plugins" />
                        </NavLink>
                        <NavLink
                            to={`${match.url}/account`}
                            className="list-group-item bg-transparent border-0"
                            activeClassName="active"
                        >
                            <Translate value="sidebar.menu.links.account" />
                        </NavLink>
                    </ListGroup>
                </div>
                <footer id="locale-selectors">
                    <LocaleSelector locale="en" />
                    <LocaleSelector locale="fr" />
                </footer>
            </aside>
        )
    }
}

Sidebar.propTypes = {
    displaySidebar: PropTypes.func.isRequired,
    hideSidebar: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
    isSidebarOpen: PropTypes.bool.isRequired,
    match: PropTypes.object.isRequired,
    user: PropTypes.object
}

const mapStateToProps = state => ({
    isAuthenticated: state.user.isAuthenticated,
    user: state.user.user,
    isSidebarOpen: state.app.isSidebarOpen
})

const mapDispatchToProps = dispatch => ({
    displaySidebar: () => dispatch(displaySidebar),
    hideSidebar: () => dispatch(hideSidebar)
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Sidebar))

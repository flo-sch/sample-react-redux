import classNames from 'classnames'
import PropTypes from 'prop-types'
import * as React from 'react'
import { connect } from 'react-redux'
import { Link, withRouter } from 'react-router-dom'

import { generateUrl } from '@/utils/UrlGenerator'

class LocaleSelector extends React.Component {
    render() {
        const { location, currentLocale, locale } = this.props

        return (
            <Link
                to={{ pathname: generateUrl(locale, location) }}
                className={classNames({
                    'btn btn-locale-selector': true,
                    'btn-primary': locale === currentLocale,
                    'btn-secondary': locale !== currentLocale
                })}
            >
                {locale}
            </Link>
        )
    }
}

LocaleSelector.propTypes = {
    currentLocale: PropTypes.string.isRequired,
    locale: PropTypes.string.isRequired,
    location: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    currentLocale: state.i18n.locale
})

export default withRouter(connect(mapStateToProps, null)(LocaleSelector))

import * as React from 'react'
import { Col, Row } from 'reactstrap'
import { Translate } from 'react-redux-i18n'

import AnalyticsGraph from '@/components/Graph/AnalyticsGraph'
import StatisticsGraph from '@/components/Graph/StatisticsGraph'

class Dashboard extends React.Component {
    render() {
        return (
            <section id="dashboard" className="section">
                <Translate tag="h2" value="dashboard.title" className="section-title" />
                <Translate
                    tag="h3"
                    value="dashboard.analytics.title"
                    className="section-subtitle"
                />
                <AnalyticsGraph />
                <Row>
                    <Col xs="12" lg="6">
                        <Translate
                            tag="h3"
                            value="dashboard.statistics.use.title"
                            className="section-subtitle"
                        />
                        <StatisticsGraph />
                    </Col>
                    <Col xs="12" lg="6">
                        <Translate
                            tag="h3"
                            value="dashboard.statistics.services.title"
                            className="section-subtitle"
                        />
                        <StatisticsGraph />
                    </Col>
                </Row>
            </section>
        )
    }
}

export default Dashboard

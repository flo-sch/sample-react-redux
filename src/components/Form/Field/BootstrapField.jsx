import PropTypes from 'prop-types'
import { isUndefined } from 'lodash'
import * as React from 'react'
import { FormFeedback, FormGroup, Input, Label } from 'reactstrap'

export default class BootstrapField extends React.Component {
    render() {
        const { groupClassName, groupStyle, input, label, meta, ...attributes } = this.props

        if (!input.id) {
            input.id = name || `field-${Math.floor(Math.random() * 1000000000)}`
        }

        let labelElement = null

        if (label) {
            labelElement = <Label htmlFor={input.id}>{label}</Label>
        }

        return (
            <FormGroup className={groupClassName} style={groupStyle}>
                {labelElement}
                <Input
                    valid={isUndefined(meta.error) || !meta.touched ? undefined : false}
                    {...input}
                    {...attributes}
                />
                {meta.touched && <FormFeedback>{meta.error}</FormFeedback>}
            </FormGroup>
        )
    }
}

BootstrapField.propTypes = {
    input: PropTypes.object.isRequired,
    meta: PropTypes.object.isRequired,
    label: PropTypes.string,
    groupClassName: PropTypes.string,
    groupStyle: PropTypes.object
}

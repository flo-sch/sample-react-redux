import classNames from 'classnames'
import PropTypes from 'prop-types'
import * as React from 'react'
import { connect } from 'react-redux'
import { I18n, Translate } from 'react-redux-i18n'
import { Button, Form } from 'reactstrap'
import { Field, reduxForm } from 'redux-form'

import { hideLoginForm } from '@/actions/User'
import BootstrapField from '@/components/Form/Field/BootstrapField'

const validate = values => {
    const errors = {}

    if (!values.email) {
        errors.email = I18n.t('home.login_form.errors.email')
    }
    if (!values.password) {
        errors.password = I18n.t('home.login_form.errors.password')
    }

    return errors
}

class LoginForm extends React.Component {
    get authenticationFeedback() {
        const { authenticationError } = this.props

        return (
            <p
                id="authentication-error"
                className={classNames({
                    'text-danger': true,
                    'is-displayed': authenticationError !== null
                })}
            >
                {authenticationError}
            </p>
        )
    }

    render() {
        const { handleSubmit, shouldLoginFormBeDisplayed, hideLoginForm } = this.props

        return (
            <Form
                id="login-form"
                className={classNames({
                    'form-inline align-items-end': true,
                    'is-displayed': shouldLoginFormBeDisplayed
                })}
                onSubmit={handleSubmit}
            >
                {this.authenticationFeedback}
                <Field
                    type="text"
                    name="email"
                    component={BootstrapField}
                    placeholder={I18n.t('home.login_form.placeholders.email')}
                    groupClassName="mb-0 mr-2"
                />
                <Field
                    type="password"
                    name="password"
                    component={BootstrapField}
                    placeholder={I18n.t('home.login_form.placeholders.password')}
                    groupClassName="mb-0 mr-2"
                />
                <div className="d-inline-block">
                    <div className="text-right my-2">
                        <Button color="link" size="lg" onClick={hideLoginForm}>
                            <Translate value="home.login_form.actions.close" />
                        </Button>
                    </div>
                    <Button type="submit" color="primary" size="lg">
                        <Translate value="home.login_form.actions.submit" />
                    </Button>
                </div>
            </Form>
        )
    }
}

LoginForm.propTypes = {
    shouldLoginFormBeDisplayed: PropTypes.bool.isRequired,
    authenticationError: PropTypes.string,
    hideLoginForm: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    shouldLoginFormBeDisplayed: state.user.displayLoginForm,
    authenticationError: state.user.error
})

const mapDispatchToProps = dispatch => ({
    hideLoginForm: () => dispatch(hideLoginForm)
})

export default reduxForm({
    form: 'loginForm',
    validate
})(connect(mapStateToProps, mapDispatchToProps)(LoginForm))

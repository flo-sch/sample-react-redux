import PropTypes from 'prop-types'
import * as React from 'react'
import { I18n } from 'react-redux-i18n'
import { Form } from 'reactstrap'
import { Field, reduxForm } from 'redux-form'

import BootstrapField from '@/components/Form/Field/BootstrapField'

class PluginsFiltersForm extends React.Component {
    render() {
        const { handleSubmit } = this.props

        return (
            <Form
                id="market-place-filters-form"
                className="form-inline justify-content-between align-items-end"
                onSubmit={handleSubmit}
            >
                <Field
                    type="select"
                    name="tags"
                    component={BootstrapField}
                    placeholder={I18n.t('market_place.filters_form.placeholders.tags')}
                    groupClassName="mb-0 mr-2"
                    className="w-100"
                    groupStyle={{
                        minWidth: '50%'
                    }}
                >
                    <option value="abc">ABC</option>
                    <option value="def">DEF</option>
                    <option value="ghi">GHI</option>
                    <option value="jkl">JKL</option>
                    <option value="mno">MNO</option>
                </Field>
                <Field
                    type="text"
                    name="query"
                    component={BootstrapField}
                    placeholder={I18n.t('market_place.filters_form.placeholders.query')}
                    groupClassName="mb-0 mr-2"
                />
            </Form>
        )
    }
}

PluginsFiltersForm.propTypes = {
    handleSubmit: PropTypes.func.isRequired
}

export default reduxForm({
    form: 'marketPlaceFiltersForm'
})(PluginsFiltersForm)

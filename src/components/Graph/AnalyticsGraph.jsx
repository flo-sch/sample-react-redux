import * as React from 'react'
import { Button, Card } from 'reactstrap'
import { Line } from 'react-chartjs-2'
import { Translate } from 'react-redux-i18n'

class AnalyticsGraph extends React.Component {
    render() {
        return (
            <Card body outline color="secondary">
                <div className="card-top-actions">
                    <Button color="primary">CSV</Button>
                    <Button color="primary">PDF</Button>
                </div>
                <Translate
                    tag="small"
                    value="dashboard.analytics.caption"
                    className="card-introduction"
                />
                <div className="chart-container">
                    <Line
                        width={960}
                        height={340}
                        data={{
                            labels: [
                                'January',
                                'February',
                                'March',
                                'April',
                                'May',
                                'June',
                                'July',
                                'August',
                                'September',
                                'October',
                                'November',
                                'December'
                            ],
                            datasets: [
                                {
                                    label: 'Something',
                                    data: [
                                        5000,
                                        6000,
                                        4500,
                                        7000,
                                        8000,
                                        9500,
                                        6000,
                                        3000,
                                        7000,
                                        7800,
                                        10100,
                                        9400
                                    ],
                                    borderColor: ['#79addc'],
                                    backgroundColor: ['#79addc'],
                                    fill: false
                                },
                                {
                                    label: 'Something else',
                                    data: [
                                        6000,
                                        2300,
                                        1100,
                                        3600,
                                        5700,
                                        8300,
                                        11200,
                                        9100,
                                        5400,
                                        5300,
                                        6800,
                                        2300
                                    ],
                                    backgroundColor: ['#ffc09f'],
                                    borderColor: ['#ffc09f'],
                                    fill: false
                                }
                            ]
                        }}
                        options={{
                            legend: {
                                labels: {
                                    boxWidth: 12,
                                    fontFamily: 'Josefin Sans, sans-serif',
                                    fontSize: 12,
                                    fontColor: '#565656'
                                }
                            },
                            scales: {
                                xAxes: [
                                    {
                                        gridLines: {
                                            display: false
                                        }
                                    }
                                ]
                            }
                        }}
                    />
                </div>
            </Card>
        )
    }
}

export default AnalyticsGraph

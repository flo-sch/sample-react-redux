import PropTypes from 'prop-types'
import Rainbow from 'rainbowvis.js'
import * as React from 'react'
import { connect } from 'react-redux'
import { Button, Card } from 'reactstrap'
import { Translate } from 'react-redux-i18n'
import { Link } from 'react-router-dom'
import Gauge from 'react-svg-gauge'

const gradient = new Rainbow()
gradient.setNumberRange(0, 100)
gradient.setSpectrum('#ffc09f', '#79addc')

class GaugeGraph extends React.Component {
    render() {
        const { locale, value } = this.props

        return (
            <Card body outline color="secondary">
                <div className="text-center" style={{ maxWidth: '300px' }}>
                    <Gauge
                        label=""
                        value={value}
                        min={0}
                        max={100}
                        color={`#${gradient.colourAt(value)}`}
                        width={300}
                        height={150}
                        valueLabelStyle={{
                            fill: `#${gradient.colourAt(value)}`
                        }}
                        topLabelStyle={{
                            fill: 'none'
                        }}
                        minMaxLabelStyle={{
                            fontSize: '12px'
                        }}
                    />
                    <Translate
                        tag="h4"
                        value="tools.stress_test.gauge.caption"
                        className="gauge-caption"
                        style={{ color: `#${gradient.colourAt(value)}` }}
                    />
                </div>
                <div className="card-bottom-actions">
                    <Button color="primary" tag={Link} to={`/${locale}/tools`}>
                        <Translate value="tools.stress_test.actions.submit" />
                    </Button>
                </div>
            </Card>
        )
    }
}

GaugeGraph.propTypes = {
    locale: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired
}

const mapStateToProps = state => ({
    locale: state.i18n.locale
})

export default connect(mapStateToProps, null)(GaugeGraph)

import PropTypes from 'prop-types'
import * as React from 'react'
import { connect } from 'react-redux'
import { Button, Card } from 'reactstrap'
import { HorizontalBar } from 'react-chartjs-2'

class StatisticsGraph extends React.Component {
    render() {
        const { onExportCSV, onExportPDF } = this.props

        return (
            <Card body outline color="secondary">
                <div className="card-top-actions">
                    <Button color="primary" onClick={onExportCSV}>
                        CSV
                    </Button>
                    <Button color="primary" onClick={onExportPDF}>
                        PDF
                    </Button>
                </div>
                <div className="chart-container">
                    <HorizontalBar
                        width={960}
                        height={480}
                        data={{
                            datasets: [
                                {
                                    label: 'Something 1',
                                    data: [8400],
                                    backgroundColor: ['#79addc'],
                                    borderColor: ['#79addc']
                                },
                                {
                                    label: 'Something 2',
                                    data: [10800],
                                    backgroundColor: ['#ffee93'],
                                    borderColor: ['#ffee93']
                                },
                                {
                                    label: 'Something 3',
                                    data: [5600],
                                    backgroundColor: ['#ffc09f'],
                                    borderColor: ['#ffc09f']
                                }
                            ]
                        }}
                        options={{
                            legend: {
                                position: 'bottom',
                                labels: {
                                    boxWidth: 12,
                                    fontFamily: 'Muli, sans-serif',
                                    fontSize: 12,
                                    fontColor: '#565656'
                                }
                            },
                            scales: {
                                xAxes: [
                                    {
                                        gridLines: {
                                            display: false
                                        }
                                    }
                                ],
                                yAxes: [
                                    {
                                        gridLines: {
                                            display: false
                                        }
                                    }
                                ]
                            }
                        }}
                    />
                </div>
            </Card>
        )
    }
}

StatisticsGraph.propTypes = {
    onExportCSV: PropTypes.func.isRequired,
    onExportPDF: PropTypes.func.isRequired
}

const mapDispatchToProps = dispatch => ({
    onExportCSV: () => {
        console.info('EXPORT CSV: TO BE IMPLEMENTED.')
    },
    onExportPDF: () => {
        console.info('EXPORT PDF: TO BE IMPLEMENTED.')
    }
})

export default connect(null, mapDispatchToProps)(StatisticsGraph)

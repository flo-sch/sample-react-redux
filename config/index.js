module.exports = {
    prod: {
        env: require('./prod.env')
    },
    dev: {
        env: require('./dev.env')
    }
}

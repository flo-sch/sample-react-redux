const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const CleanPlugin = require('clean-webpack-plugin')
const HtmlPlugin = require('html-webpack-plugin')

/**
 * Import build config
 */
const config = require('./config')

const PUBLIC_FOLDER = path.resolve(__dirname, 'public')
const BUILD_FOLDER = path.resolve(__dirname, 'build')
const SRC_FOLDER = path.resolve(__dirname, 'src')
const ASSETS_FOLDER = path.resolve(SRC_FOLDER, 'assets')

module.exports = {
    entry: ['react-hot-loader/patch', './src/main.jsx'],
    output: {
        filename: '[name].js',
        path: BUILD_FOLDER,
        publicPath: '/'
    },
    resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx', '.es6', '.json', '.scss', '.css'],
        alias: {
            '@': SRC_FOLDER
        }
    },
    devServer: {
        publicPath: '/',
        contentBase: PUBLIC_FOLDER,
        compress: true,
        historyApiFallback: true,
        hot: true,
        watchOptions: {
            ignored: /node_modules/
        }
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx|ts|tsx|es6)$/,
                loader: ['react-hot-loader/webpack', 'babel-loader'],
                include: [SRC_FOLDER],
                exclude: ['/node_modules/']
            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/i,
                loader: 'url-loader',
                options: {
                    limit: 10000
                }
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'style-loader' // Create style node from JS string
                    },
                    {
                        loader: 'css-loader' // Translate CSS into CommonJS
                    },
                    {
                        loader: 'sass-loader' // Compile SASS to CSS
                        // options: {
                        //     includePaths: []
                        // }
                    }
                ]
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                loader: 'url-loader',
                options: {
                    limit: 10000
                }
            }
        ]
    },
    plugins: [
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en-gb|fr/),
        new webpack.DefinePlugin({
            'process.env': process.env.NODE_ENV === 'production' ? config.prod.env : config.dev.env
        }),
        new CleanPlugin([BUILD_FOLDER], {
            exclude: ['.gitignore']
        }),
        new HtmlPlugin({
            filename: 'index.html',
            template: path.resolve(PUBLIC_FOLDER, 'index.html'),
            inject: true
        }),
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.BannerPlugin({
            banner: '// { "framework": "React" }\n',
            raw: true
        }),
        new ExtractTextPlugin({
            filename: '[name].[contenthash].css'
        })
    ]
}

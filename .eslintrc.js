/**
 * ESLint configuration
 *
 * @see http://eslint.org/docs/user-guide/configuring
 */
module.exports = {
    root: true,
    parser: 'babel-eslint',
    parserOptions: {
        sourceType: 'module'
    },
    extends: [
        'standard',
        'plugin:react/recommended',
        'prettier',
        'prettier/react',
        'prettier/standard'
    ],
    plugins: ['html', 'prettier', 'react', 'standard'],
    parserOptions: {
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true
        }
    },
    settings: {
        'import/resolver': {
            webpack: {
                config: 'webpack.config.js'
            }
        }
    },
    rules: {
        'prettier/prettier': 'error'
    },
    env: {
        es6: true,
        browser: true,
        node: false
    },
    globals: {
        window: true,
        module: true,
        process: true
    }
}

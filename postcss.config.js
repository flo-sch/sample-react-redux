/**
 * PostCSS Configuration
 *
 * @see http://postcss.org/
 */

module.exports = {
    plugins: {
        // to edit target browsers: use 'browserlist' field in package.json
        autoprefixer: {}
    }
}
